// ====================================================================
// Edyta PRZYMUS
// Allan CORNET - DIGITEO - 2011
// ====================================================================
function builder_src_cpp()

  src_cpp_path = get_absolute_file_path('builder_cpp.sce');
  CPPFLAGS = "";
  LDFLAGS = ""

  if getos() == "Windows" then
  else
    CPPFLAGS = "-frounding-math -I" + src_cpp_path;
    LDFLAGS = '-lCGAL -lgmp -lCGAL_ImageIO';
  end

  files_cpp = ['Constrained_Delaunay.cpp', ..
         'convex_hull.cpp', ..
         'Delaunay_2d.cpp', ..
         'Delaunay_3d.cpp', ..
         'Delaunay_mesher.cpp', ..
         'Delaunay_nd.cpp', ..
         'interpolation_functions.cpp', ..
         'Stream_lines.cpp', ..
         'Surface_mesher.cpp', ..
         'scilab_job.c'] ;

   tbx_build_src('cgal_cpp', ..
                 files_cpp, ..
                 'cpp', ..
                 src_cpp_path, ..
                 '', ..
                 LDFLAGS, ..
                 CPPFLAGS);
endfunction

builder_src_cpp();
clear builder_src_cpp;
