// Copyright (C) 2011 - Edyta Przymus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//checking what error will be produced with wrong types of inputs
x = rand(1,10);
y = rand(1,10);
[tri,ptr] = delaunay_2(x,y);
new_x = rand(1,10);
new_y = rand(1,10);
assert_checkerror("dt2_remove_points(x,x,y)","%s: Wrong type for input argument #%d: A pointer expected.",999,"dt2_remove_points",1);
assert_checkerror("dt2_remove_points(ptr,ptr,y)","%s: Wrong type for input argument #%d: A vector expected.",999,"dt2_remove_points",2);
assert_checkerror("dt2_remove_points(ptr,x,ptr)","%s: Wrong type for input argument #%d: A vector expected.",999,"dt2_remove_points",3);

//checking what error will be produced with wrong number of inputs
assert_checkerror("dt2_remove_points(ptr,x)","%s: Wrong number of input argument(s): %d expected.",77,"dt2_remove_points",3);

//testing what error message is produced when inputs have wrong size
x = rand(1,10);
y = rand(1,10);
[tri1,ptr] = delaunay_2(x,y);
new_x = rand(1,10);
new_y = rand(1,8);  
assert_checkerror("dt2_remove_points(ptr,new_x,new_y)","%s: Incompatible inputs.",999,"dt2_remove_points");

//testing if computed and expected outputs are the same
x=[ 0.9058    0.1270    0.9134    0.6324   0.0975  0.9058    0.1270    0.9134    0.6324    0.0975];
y=[ 0.9706    0.9572    0.4854    0.8003   0.1419  0.4218    0.9157    0.7922    0.9595    0.6557];
[tri, ptr]=delaunay_2(x,y);
xx=[ 0.9058   0.1270];
yy=[ 0.9706   0.9572];
dt2_remove_points(ptr,xx,yy);
new_tri = dt2_get_connectivity(ptr);
new_tri1=int32([5  1  8;7  1  5;6  3  2;2  3  4;3  5  8;3  7  5;7  3  6;3  8  4]);
assert_checkequal(new_tri,new_tri1);
